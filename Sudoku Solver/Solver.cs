﻿using System.Threading;

namespace Sudoku_Solver
{
    public static class Solver
    {
        public static int Iteration { get; private set; }

        //Extension method
        public static bool Solve(this int[,] board)
        {
            var status = new int[board.Length, board.Length];
            Iteration = 1;
            //Populate the array from passed in board
            for (var i = 0; i < Constants.BoardSize; i++)
            {
                for (var j = 0; j < Constants.BoardSize; j++)
                {
                    //If the cell has a number, set state to 2
                    //If the cell has no number, set state to 0
                    status[i, j] = board[i, j] > 0 ? 2 : 0;
                }
            }
            return RecursiveAlgorithm(board, status, 0, 0);
        }

        /// <summary>
        /// Solves the board recursively and returns a bool whether it's solved or not
        /// </summary>
        /// <param name="board">The board being solved</param>
        /// <param name="status">The status of each cell</param>
        /// <param name="column">Initial column</param>
        /// <param name="row">Initial row</param>
        /// <returns>flag stays true if Solution is solvable</returns>
        private static bool RecursiveAlgorithm(int[,] board, int[,] status, int column, int row)
        {
            //Recursive 
            while (true)
            {
                if (column == Constants.BoardSize)
                {
                    var currentCell = 0;
                    for (var i = 0; i < Constants.BoardSize; i++)
                    {
                        for (var j = 0; j < Constants.BoardSize; j++)
                        {
                            currentCell += status[i, j] > 0 ? 1 : 0;
                        }
                    }
                    return currentCell == Constants.BoardSize*Constants.BoardSize;
                }

                if (status[column, row] >= 1)
                {
                    var nextColumn = column;
                    var nextRow = row + 1;
                    if (nextRow == Constants.BoardSize)
                    {
                        nextColumn = column + 1;
                        nextRow = 0;
                    }

                    column = nextColumn;
                    row = nextRow;
                    continue;
                }

                var used = new bool[Constants.BoardSize];
                for (var i = 0; i < Constants.BoardSize; i++)
                {
                    if (status[column, i] >= 1)
                    {
                        used[board[column, i] - 1] = true;
                    }
                    if (status[i, row] >= 1)
                    {
                        used[board[i, row] - 1] = true;
                    }
                }

                for (var i = column - (column%3); i < column - (column%3) + 3; i++)
                {
                    for (var j = row - (row%3); j < row - (row%3) + 3; j++)
                    {
                        if (status[i, j] >= 1)
                        {
                            used[board[i, j] - 1] = true;
                        }
                    }
                }

                for (var i = 0; i < used.Length; i++)
                {
                    if (!used[i])
                    {
                        status[column, row] = 1;
                        board[column, row] = i + 1;

                        var nextX = column;
                        var nextY = row + 1;

                        if (nextY == Constants.BoardSize)
                        {
                            nextX = column + 1;
                            nextY = 0;
                        }

                        if (RecursiveAlgorithm(board, status, nextX, nextY))
                        {
                            return true;
                        }

                        status[column, row] = 0;
                        board[column, row] = 0;
                        Iteration++;
                    }
                }
                GiveToGui(board);
                return false;
            }
        }

        private static void GiveToGui(int[,] board)
        {
            if (Form1.DoIterations)
            {
                if (Iteration%Form1.IterationResolution == 0)
                {
                    board.PrintSolutionToConsole(Iteration);
                    Thread.Sleep(Form1.DelayBetweenIterations);

                    //Always report the progress
                    Form1.WorkerThread.ReportProgress(1);
                }
            }
        }
    }
}