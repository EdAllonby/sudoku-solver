﻿using System.Diagnostics;
using System.Text;

namespace Sudoku_Solver
{
    public static class OutputConsole
    {
        public static int Iteration { get; private set; }

        public static void PrintSolutionToConsole(this int[,] board, int tempIteration = 0, bool newPuzzle = false)
        {
            var sudokuLog = new StringBuilder(512);

            //Initialise iteration variable if newPuzzle flag is set to true
            if (newPuzzle)
            {
                Iteration = 0;
            }

            if (tempIteration > Iteration)
            {
                Iteration = tempIteration;
            }

            for (var i = 0; i < Constants.BoardSize; i++)
            {
                if (i%3 == 0 && i != 0)
                {
                    sudokuLog.AppendLine("----------------------");
                }

                for (var j = 0; j < Constants.BoardSize; j++)
                {
                    if (j%3 == 0 && j != 0)
                    {
                        sudokuLog.Append("| ");
                    }

                    sudokuLog.Append(board[i, j] + " ");
                }

                sudokuLog.AppendLine("");
            }
            sudokuLog.AppendLine("")
                .AppendLine("Iteration: " + Iteration)
                .AppendLine("");

            Debug.WriteLine(sudokuLog.ToString());
        }
    }
}