﻿namespace Sudoku_Solver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boardToSolveGroup = new gGlowBox.gGlowGroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.solvePuzzleButton = new System.Windows.Forms.Button();
            this.presetBoardSelector = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sudokuCompleteLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.validationResultsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.iterationLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solvedPuzzleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.delayBetweenIterationsTrackBar = new System.Windows.Forms.TrackBar();
            this.iterationResolutionTrackBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.delayBetweenIterationsLabel = new System.Windows.Forms.Label();
            this.iterationResolutionLabel = new System.Windows.Forms.Label();
            this.showIterationsCheckBox = new System.Windows.Forms.CheckBox();
            this.boardToSolveGroup.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayBetweenIterationsTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iterationResolutionTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // boardToSolveGroup
            // 
            this.boardToSolveGroup.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.boardToSolveGroup.Controls.Add(this.textBox1);
            this.boardToSolveGroup.Controls.Add(this.textBox2);
            this.boardToSolveGroup.Controls.Add(this.textBox3);
            this.boardToSolveGroup.Controls.Add(this.textBox4);
            this.boardToSolveGroup.Controls.Add(this.textBox5);
            this.boardToSolveGroup.Controls.Add(this.textBox6);
            this.boardToSolveGroup.Controls.Add(this.textBox7);
            this.boardToSolveGroup.Controls.Add(this.textBox8);
            this.boardToSolveGroup.Controls.Add(this.textBox9);
            this.boardToSolveGroup.Controls.Add(this.textBox10);
            this.boardToSolveGroup.Controls.Add(this.textBox11);
            this.boardToSolveGroup.Controls.Add(this.textBox12);
            this.boardToSolveGroup.Controls.Add(this.textBox13);
            this.boardToSolveGroup.Controls.Add(this.textBox14);
            this.boardToSolveGroup.Controls.Add(this.textBox15);
            this.boardToSolveGroup.Controls.Add(this.textBox16);
            this.boardToSolveGroup.Controls.Add(this.textBox17);
            this.boardToSolveGroup.Controls.Add(this.textBox18);
            this.boardToSolveGroup.Controls.Add(this.textBox19);
            this.boardToSolveGroup.Controls.Add(this.textBox20);
            this.boardToSolveGroup.Controls.Add(this.textBox21);
            this.boardToSolveGroup.Controls.Add(this.textBox22);
            this.boardToSolveGroup.Controls.Add(this.textBox23);
            this.boardToSolveGroup.Controls.Add(this.textBox24);
            this.boardToSolveGroup.Controls.Add(this.textBox25);
            this.boardToSolveGroup.Controls.Add(this.textBox26);
            this.boardToSolveGroup.Controls.Add(this.textBox27);
            this.boardToSolveGroup.Controls.Add(this.textBox28);
            this.boardToSolveGroup.Controls.Add(this.textBox29);
            this.boardToSolveGroup.Controls.Add(this.textBox30);
            this.boardToSolveGroup.Controls.Add(this.textBox31);
            this.boardToSolveGroup.Controls.Add(this.textBox32);
            this.boardToSolveGroup.Controls.Add(this.textBox33);
            this.boardToSolveGroup.Controls.Add(this.textBox34);
            this.boardToSolveGroup.Controls.Add(this.textBox35);
            this.boardToSolveGroup.Controls.Add(this.textBox36);
            this.boardToSolveGroup.Controls.Add(this.textBox37);
            this.boardToSolveGroup.Controls.Add(this.textBox38);
            this.boardToSolveGroup.Controls.Add(this.textBox39);
            this.boardToSolveGroup.Controls.Add(this.textBox40);
            this.boardToSolveGroup.Controls.Add(this.textBox41);
            this.boardToSolveGroup.Controls.Add(this.textBox42);
            this.boardToSolveGroup.Controls.Add(this.textBox43);
            this.boardToSolveGroup.Controls.Add(this.textBox44);
            this.boardToSolveGroup.Controls.Add(this.textBox45);
            this.boardToSolveGroup.Controls.Add(this.textBox46);
            this.boardToSolveGroup.Controls.Add(this.textBox47);
            this.boardToSolveGroup.Controls.Add(this.textBox48);
            this.boardToSolveGroup.Controls.Add(this.textBox49);
            this.boardToSolveGroup.Controls.Add(this.textBox50);
            this.boardToSolveGroup.Controls.Add(this.textBox51);
            this.boardToSolveGroup.Controls.Add(this.textBox52);
            this.boardToSolveGroup.Controls.Add(this.textBox53);
            this.boardToSolveGroup.Controls.Add(this.textBox54);
            this.boardToSolveGroup.Controls.Add(this.textBox55);
            this.boardToSolveGroup.Controls.Add(this.textBox56);
            this.boardToSolveGroup.Controls.Add(this.textBox57);
            this.boardToSolveGroup.Controls.Add(this.textBox58);
            this.boardToSolveGroup.Controls.Add(this.textBox59);
            this.boardToSolveGroup.Controls.Add(this.textBox60);
            this.boardToSolveGroup.Controls.Add(this.textBox61);
            this.boardToSolveGroup.Controls.Add(this.textBox62);
            this.boardToSolveGroup.Controls.Add(this.textBox63);
            this.boardToSolveGroup.Controls.Add(this.textBox64);
            this.boardToSolveGroup.Controls.Add(this.textBox65);
            this.boardToSolveGroup.Controls.Add(this.textBox66);
            this.boardToSolveGroup.Controls.Add(this.textBox67);
            this.boardToSolveGroup.Controls.Add(this.textBox68);
            this.boardToSolveGroup.Controls.Add(this.textBox69);
            this.boardToSolveGroup.Controls.Add(this.textBox70);
            this.boardToSolveGroup.Controls.Add(this.textBox71);
            this.boardToSolveGroup.Controls.Add(this.textBox72);
            this.boardToSolveGroup.Controls.Add(this.textBox73);
            this.boardToSolveGroup.Controls.Add(this.textBox74);
            this.boardToSolveGroup.Controls.Add(this.textBox75);
            this.boardToSolveGroup.Controls.Add(this.textBox76);
            this.boardToSolveGroup.Controls.Add(this.textBox77);
            this.boardToSolveGroup.Controls.Add(this.textBox78);
            this.boardToSolveGroup.Controls.Add(this.textBox79);
            this.boardToSolveGroup.Controls.Add(this.textBox80);
            this.boardToSolveGroup.Controls.Add(this.textBox81);
            this.boardToSolveGroup.Location = new System.Drawing.Point(43, 103);
            this.boardToSolveGroup.Name = "boardToSolveGroup";
            this.boardToSolveGroup.Size = new System.Drawing.Size(272, 289);
            this.boardToSolveGroup.TabIndex = 0;
            this.boardToSolveGroup.Text = "Board To Solve";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(16, 30);
            this.textBox1.MaxLength = 1;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(20, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(42, 30);
            this.textBox2.MaxLength = 1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(20, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(68, 30);
            this.textBox3.MaxLength = 1;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(20, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(100, 30);
            this.textBox4.MaxLength = 1;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(20, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(126, 30);
            this.textBox5.MaxLength = 1;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(20, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(152, 30);
            this.textBox6.MaxLength = 1;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(20, 20);
            this.textBox6.TabIndex = 5;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(184, 30);
            this.textBox7.MaxLength = 1;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(20, 20);
            this.textBox7.TabIndex = 6;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(210, 30);
            this.textBox8.MaxLength = 1;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(20, 20);
            this.textBox8.TabIndex = 7;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(236, 30);
            this.textBox9.MaxLength = 1;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(20, 20);
            this.textBox9.TabIndex = 8;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(16, 56);
            this.textBox10.MaxLength = 1;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(20, 20);
            this.textBox10.TabIndex = 9;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(42, 56);
            this.textBox11.MaxLength = 1;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(20, 20);
            this.textBox11.TabIndex = 10;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(68, 56);
            this.textBox12.MaxLength = 1;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(20, 20);
            this.textBox12.TabIndex = 11;
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(100, 56);
            this.textBox13.MaxLength = 1;
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(20, 20);
            this.textBox13.TabIndex = 12;
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(126, 56);
            this.textBox14.MaxLength = 1;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(20, 20);
            this.textBox14.TabIndex = 13;
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(152, 56);
            this.textBox15.MaxLength = 1;
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(20, 20);
            this.textBox15.TabIndex = 14;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox15.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(184, 56);
            this.textBox16.MaxLength = 1;
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(20, 20);
            this.textBox16.TabIndex = 15;
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox16.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(210, 56);
            this.textBox17.MaxLength = 1;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(20, 20);
            this.textBox17.TabIndex = 16;
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(236, 56);
            this.textBox18.MaxLength = 1;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(20, 20);
            this.textBox18.TabIndex = 17;
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox18.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(16, 82);
            this.textBox19.MaxLength = 1;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(20, 20);
            this.textBox19.TabIndex = 18;
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox19.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(42, 82);
            this.textBox20.MaxLength = 1;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(20, 20);
            this.textBox20.TabIndex = 19;
            this.textBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(68, 82);
            this.textBox21.MaxLength = 1;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(20, 20);
            this.textBox21.TabIndex = 20;
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(100, 82);
            this.textBox22.MaxLength = 1;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(20, 20);
            this.textBox22.TabIndex = 21;
            this.textBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(126, 82);
            this.textBox23.MaxLength = 1;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(20, 20);
            this.textBox23.TabIndex = 22;
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox23.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(152, 82);
            this.textBox24.MaxLength = 1;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(20, 20);
            this.textBox24.TabIndex = 23;
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox24.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(184, 82);
            this.textBox25.MaxLength = 1;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(20, 20);
            this.textBox25.TabIndex = 24;
            this.textBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox25.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(210, 82);
            this.textBox26.MaxLength = 1;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(20, 20);
            this.textBox26.TabIndex = 25;
            this.textBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox26.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(236, 82);
            this.textBox27.MaxLength = 1;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(20, 20);
            this.textBox27.TabIndex = 26;
            this.textBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox27.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(16, 114);
            this.textBox28.MaxLength = 1;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(20, 20);
            this.textBox28.TabIndex = 27;
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox28.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(42, 114);
            this.textBox29.MaxLength = 1;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(20, 20);
            this.textBox29.TabIndex = 28;
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox29.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(68, 114);
            this.textBox30.MaxLength = 1;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(20, 20);
            this.textBox30.TabIndex = 29;
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox30.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(100, 114);
            this.textBox31.MaxLength = 1;
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(20, 20);
            this.textBox31.TabIndex = 30;
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(126, 114);
            this.textBox32.MaxLength = 1;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(20, 20);
            this.textBox32.TabIndex = 31;
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(152, 114);
            this.textBox33.MaxLength = 1;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(20, 20);
            this.textBox33.TabIndex = 32;
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox33.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(184, 114);
            this.textBox34.MaxLength = 1;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(20, 20);
            this.textBox34.TabIndex = 33;
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox34.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(210, 114);
            this.textBox35.MaxLength = 1;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(20, 20);
            this.textBox35.TabIndex = 34;
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox35.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(236, 114);
            this.textBox36.MaxLength = 1;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(20, 20);
            this.textBox36.TabIndex = 35;
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox36.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(16, 140);
            this.textBox37.MaxLength = 1;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(20, 20);
            this.textBox37.TabIndex = 36;
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox37.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(42, 140);
            this.textBox38.MaxLength = 1;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(20, 20);
            this.textBox38.TabIndex = 37;
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox38.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(68, 140);
            this.textBox39.MaxLength = 1;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(20, 20);
            this.textBox39.TabIndex = 38;
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox39.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(100, 140);
            this.textBox40.MaxLength = 1;
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(20, 20);
            this.textBox40.TabIndex = 39;
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox40.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(126, 140);
            this.textBox41.MaxLength = 1;
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(20, 20);
            this.textBox41.TabIndex = 40;
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(152, 140);
            this.textBox42.MaxLength = 1;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(20, 20);
            this.textBox42.TabIndex = 41;
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(184, 140);
            this.textBox43.MaxLength = 1;
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(20, 20);
            this.textBox43.TabIndex = 42;
            this.textBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox43.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(210, 140);
            this.textBox44.MaxLength = 1;
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(20, 20);
            this.textBox44.TabIndex = 43;
            this.textBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox44.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(236, 140);
            this.textBox45.MaxLength = 1;
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(20, 20);
            this.textBox45.TabIndex = 44;
            this.textBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox45.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(16, 166);
            this.textBox46.MaxLength = 1;
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(20, 20);
            this.textBox46.TabIndex = 45;
            this.textBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox46.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(42, 166);
            this.textBox47.MaxLength = 1;
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(20, 20);
            this.textBox47.TabIndex = 46;
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox47.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(68, 166);
            this.textBox48.MaxLength = 1;
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(20, 20);
            this.textBox48.TabIndex = 47;
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox48.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(100, 166);
            this.textBox49.MaxLength = 1;
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(20, 20);
            this.textBox49.TabIndex = 48;
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox49.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(126, 166);
            this.textBox50.MaxLength = 1;
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(20, 20);
            this.textBox50.TabIndex = 49;
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(152, 166);
            this.textBox51.MaxLength = 1;
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(20, 20);
            this.textBox51.TabIndex = 50;
            this.textBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox51.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(184, 166);
            this.textBox52.MaxLength = 1;
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(20, 20);
            this.textBox52.TabIndex = 51;
            this.textBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox52.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(210, 166);
            this.textBox53.MaxLength = 1;
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(20, 20);
            this.textBox53.TabIndex = 52;
            this.textBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox53.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(236, 166);
            this.textBox54.MaxLength = 1;
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(20, 20);
            this.textBox54.TabIndex = 53;
            this.textBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox54.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(16, 198);
            this.textBox55.MaxLength = 1;
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(20, 20);
            this.textBox55.TabIndex = 54;
            this.textBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox55.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(42, 198);
            this.textBox56.MaxLength = 1;
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(20, 20);
            this.textBox56.TabIndex = 55;
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox56.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(68, 198);
            this.textBox57.MaxLength = 1;
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(20, 20);
            this.textBox57.TabIndex = 56;
            this.textBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox57.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(100, 198);
            this.textBox58.MaxLength = 1;
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(20, 20);
            this.textBox58.TabIndex = 57;
            this.textBox58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox58.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(126, 198);
            this.textBox59.MaxLength = 1;
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(20, 20);
            this.textBox59.TabIndex = 58;
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox59.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(152, 198);
            this.textBox60.MaxLength = 1;
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(20, 20);
            this.textBox60.TabIndex = 59;
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox60.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(184, 198);
            this.textBox61.MaxLength = 1;
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(20, 20);
            this.textBox61.TabIndex = 60;
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox61.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(210, 198);
            this.textBox62.MaxLength = 1;
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(20, 20);
            this.textBox62.TabIndex = 61;
            this.textBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox62.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(236, 198);
            this.textBox63.MaxLength = 1;
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(20, 20);
            this.textBox63.TabIndex = 62;
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox63.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(16, 224);
            this.textBox64.MaxLength = 1;
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(20, 20);
            this.textBox64.TabIndex = 63;
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox64.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(42, 224);
            this.textBox65.MaxLength = 1;
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(20, 20);
            this.textBox65.TabIndex = 64;
            this.textBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox65.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(68, 224);
            this.textBox66.MaxLength = 1;
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(20, 20);
            this.textBox66.TabIndex = 65;
            this.textBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox66.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(100, 224);
            this.textBox67.MaxLength = 1;
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(20, 20);
            this.textBox67.TabIndex = 66;
            this.textBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox67.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(126, 224);
            this.textBox68.MaxLength = 1;
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(20, 20);
            this.textBox68.TabIndex = 67;
            this.textBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox68.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(152, 224);
            this.textBox69.MaxLength = 1;
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(20, 20);
            this.textBox69.TabIndex = 68;
            this.textBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox69.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox70
            // 
            this.textBox70.Location = new System.Drawing.Point(184, 224);
            this.textBox70.MaxLength = 1;
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(20, 20);
            this.textBox70.TabIndex = 69;
            this.textBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox70.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(210, 224);
            this.textBox71.MaxLength = 1;
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(20, 20);
            this.textBox71.TabIndex = 70;
            this.textBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox71.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox72
            // 
            this.textBox72.Location = new System.Drawing.Point(236, 224);
            this.textBox72.MaxLength = 1;
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(20, 20);
            this.textBox72.TabIndex = 71;
            this.textBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox72.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox73
            // 
            this.textBox73.Location = new System.Drawing.Point(16, 250);
            this.textBox73.MaxLength = 1;
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(20, 20);
            this.textBox73.TabIndex = 72;
            this.textBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox73.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox74
            // 
            this.textBox74.Location = new System.Drawing.Point(42, 250);
            this.textBox74.MaxLength = 1;
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(20, 20);
            this.textBox74.TabIndex = 73;
            this.textBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox74.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox75
            // 
            this.textBox75.Location = new System.Drawing.Point(68, 250);
            this.textBox75.MaxLength = 1;
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(20, 20);
            this.textBox75.TabIndex = 74;
            this.textBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox75.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox76
            // 
            this.textBox76.Location = new System.Drawing.Point(100, 250);
            this.textBox76.MaxLength = 1;
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(20, 20);
            this.textBox76.TabIndex = 75;
            this.textBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox76.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox77
            // 
            this.textBox77.Location = new System.Drawing.Point(126, 250);
            this.textBox77.MaxLength = 1;
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(20, 20);
            this.textBox77.TabIndex = 76;
            this.textBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox77.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox78
            // 
            this.textBox78.Location = new System.Drawing.Point(152, 250);
            this.textBox78.MaxLength = 1;
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(20, 20);
            this.textBox78.TabIndex = 77;
            this.textBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox78.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox79
            // 
            this.textBox79.Location = new System.Drawing.Point(184, 250);
            this.textBox79.MaxLength = 1;
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(20, 20);
            this.textBox79.TabIndex = 78;
            this.textBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox79.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox80
            // 
            this.textBox80.Location = new System.Drawing.Point(210, 250);
            this.textBox80.MaxLength = 1;
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(20, 20);
            this.textBox80.TabIndex = 79;
            this.textBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox80.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // textBox81
            // 
            this.textBox81.Location = new System.Drawing.Point(236, 250);
            this.textBox81.MaxLength = 1;
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(20, 20);
            this.textBox81.TabIndex = 80;
            this.textBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox81.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CellValidation);
            // 
            // solvePuzzleButton
            // 
            this.solvePuzzleButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.solvePuzzleButton.Location = new System.Drawing.Point(143, 413);
            this.solvePuzzleButton.Name = "solvePuzzleButton";
            this.solvePuzzleButton.Size = new System.Drawing.Size(72, 23);
            this.solvePuzzleButton.TabIndex = 1;
            this.solvePuzzleButton.Text = "Solve!";
            this.solvePuzzleButton.UseVisualStyleBackColor = true;
            this.solvePuzzleButton.Click += new System.EventHandler(this.SolvePuzzle);
            // 
            // presetBoardSelector
            // 
            this.presetBoardSelector.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.presetBoardSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.presetBoardSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.presetBoardSelector.FormattingEnabled = true;
            this.presetBoardSelector.Items.AddRange(new object[] {
            "Easy Puzzle",
            "Hard Puzzle",
            "Empty Puzzle",
            "Unsolvable Puzzle"});
            this.presetBoardSelector.Location = new System.Drawing.Point(121, 34);
            this.presetBoardSelector.Name = "presetBoardSelector";
            this.presetBoardSelector.Size = new System.Drawing.Size(121, 21);
            this.presetBoardSelector.TabIndex = 2;
            this.presetBoardSelector.SelectedIndexChanged += new System.EventHandler(this.SelectPresetBoard);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sudokuCompleteLabel,
            this.validationResultsLabel,
            this.toolStripStatusLabel1,
            this.iterationLabel,
            this.toolStripStatusLabel2,
            this.timeLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 449);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(354, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sudokuCompleteLabel
            // 
            this.sudokuCompleteLabel.Name = "sudokuCompleteLabel";
            this.sudokuCompleteLabel.Size = new System.Drawing.Size(98, 17);
            this.sudokuCompleteLabel.Text = "sudokuComplete";
            // 
            // validationResultsLabel
            // 
            this.validationResultsLabel.Name = "validationResultsLabel";
            this.validationResultsLabel.Size = new System.Drawing.Size(96, 17);
            this.validationResultsLabel.Text = "validationResults";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(86, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // iterationLabel
            // 
            this.iterationLabel.Name = "iterationLabel";
            this.iterationLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(4, 17);
            // 
            // timeLabel
            // 
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(55, 17);
            this.timeLabel.Text = "                ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.helpStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(354, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importBoardToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuItem.Text = "File";
            // 
            // importBoardToolStripMenuItem
            // 
            this.importBoardToolStripMenuItem.Name = "importBoardToolStripMenuItem";
            this.importBoardToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.importBoardToolStripMenuItem.Text = "Import Board...";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outputToLogToolStripMenuItem,
            this.solvedPuzzleToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            // 
            // outputToLogToolStripMenuItem
            // 
            this.outputToLogToolStripMenuItem.Name = "outputToLogToolStripMenuItem";
            this.outputToLogToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.outputToLogToolStripMenuItem.Text = "Output To Log...";
            // 
            // solvedPuzzleToolStripMenuItem
            // 
            this.solvedPuzzleToolStripMenuItem.Name = "solvedPuzzleToolStripMenuItem";
            this.solvedPuzzleToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.solvedPuzzleToolStripMenuItem.Text = "Solved Puzzle...";
            // 
            // helpStripMenuItem
            // 
            this.helpStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToToolStripMenuItem});
            this.helpStripMenuItem.Name = "helpStripMenuItem";
            this.helpStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpStripMenuItem.Text = "Help";
            // 
            // howToToolStripMenuItem
            // 
            this.howToToolStripMenuItem.Name = "howToToolStripMenuItem";
            this.howToToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.howToToolStripMenuItem.Text = "How To";
            // 
            // delayBetweenIterationsTrackBar
            // 
            this.delayBetweenIterationsTrackBar.AutoSize = false;
            this.delayBetweenIterationsTrackBar.Location = new System.Drawing.Point(157, 58);
            this.delayBetweenIterationsTrackBar.Maximum = 1000;
            this.delayBetweenIterationsTrackBar.Minimum = 10;
            this.delayBetweenIterationsTrackBar.Name = "delayBetweenIterationsTrackBar";
            this.delayBetweenIterationsTrackBar.Size = new System.Drawing.Size(113, 20);
            this.delayBetweenIterationsTrackBar.TabIndex = 8;
            this.delayBetweenIterationsTrackBar.TabStop = false;
            this.delayBetweenIterationsTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.delayBetweenIterationsTrackBar.Value = 200;
            this.delayBetweenIterationsTrackBar.ValueChanged += new System.EventHandler(this.SleepTrackBarValueChanged);
            // 
            // iterationResolutionTrackBar
            // 
            this.iterationResolutionTrackBar.AutoSize = false;
            this.iterationResolutionTrackBar.Location = new System.Drawing.Point(157, 77);
            this.iterationResolutionTrackBar.Maximum = 1000;
            this.iterationResolutionTrackBar.Minimum = 1;
            this.iterationResolutionTrackBar.Name = "iterationResolutionTrackBar";
            this.iterationResolutionTrackBar.Size = new System.Drawing.Size(113, 20);
            this.iterationResolutionTrackBar.TabIndex = 9;
            this.iterationResolutionTrackBar.TabStop = false;
            this.iterationResolutionTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.iterationResolutionTrackBar.Value = 1;
            this.iterationResolutionTrackBar.ValueChanged += new System.EventHandler(this.ResolutionTrackBarValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Delay between iterations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Iteration resolution";
            // 
            // delayBetweenIterationsLabel
            // 
            this.delayBetweenIterationsLabel.AutoSize = true;
            this.delayBetweenIterationsLabel.Location = new System.Drawing.Point(272, 62);
            this.delayBetweenIterationsLabel.Name = "delayBetweenIterationsLabel";
            this.delayBetweenIterationsLabel.Size = new System.Drawing.Size(20, 13);
            this.delayBetweenIterationsLabel.TabIndex = 12;
            this.delayBetweenIterationsLabel.Text = "ms";
            // 
            // iterationResolutionLabel
            // 
            this.iterationResolutionLabel.AutoSize = true;
            this.iterationResolutionLabel.Location = new System.Drawing.Point(272, 81);
            this.iterationResolutionLabel.Name = "iterationResolutionLabel";
            this.iterationResolutionLabel.Size = new System.Drawing.Size(27, 13);
            this.iterationResolutionLabel.TabIndex = 13;
            this.iterationResolutionLabel.Text = "1 / r";
            // 
            // showIterationsCheckBox
            // 
            this.showIterationsCheckBox.AutoSize = true;
            this.showIterationsCheckBox.Checked = true;
            this.showIterationsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showIterationsCheckBox.Location = new System.Drawing.Point(224, 416);
            this.showIterationsCheckBox.Name = "showIterationsCheckBox";
            this.showIterationsCheckBox.Size = new System.Drawing.Size(105, 17);
            this.showIterationsCheckBox.TabIndex = 14;
            this.showIterationsCheckBox.Text = "Show Iterations?";
            this.showIterationsCheckBox.UseVisualStyleBackColor = true;
            this.showIterationsCheckBox.CheckedChanged += new System.EventHandler(this.ShowIterationsCheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 471);
            this.Controls.Add(this.showIterationsCheckBox);
            this.Controls.Add(this.iterationResolutionLabel);
            this.Controls.Add(this.delayBetweenIterationsLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.iterationResolutionTrackBar);
            this.Controls.Add(this.delayBetweenIterationsTrackBar);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.presetBoardSelector);
            this.Controls.Add(this.solvePuzzleButton);
            this.Controls.Add(this.boardToSolveGroup);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(370, 400);
            this.Name = "Form1";
            this.Text = "Ed\'s Sudoku Solver";
            this.boardToSolveGroup.ResumeLayout(false);
            this.boardToSolveGroup.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayBetweenIterationsTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iterationResolutionTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private gGlowBox.gGlowGroupBox boardToSolveGroup;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.Button solvePuzzleButton;
        private System.Windows.Forms.ComboBox presetBoardSelector;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel sudokuCompleteLabel;
        private System.Windows.Forms.ToolStripStatusLabel validationResultsLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importBoardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputToLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solvedPuzzleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel timeLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel iterationLabel;
        private System.Windows.Forms.TrackBar delayBetweenIterationsTrackBar;
        private System.Windows.Forms.TrackBar iterationResolutionTrackBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label delayBetweenIterationsLabel;
        private System.Windows.Forms.Label iterationResolutionLabel;
        private System.Windows.Forms.CheckBox showIterationsCheckBox;
    }
}