﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sudoku_Solver
{
    public static class Validation
    {
        private static bool isNotValid;

        public static bool DoChecks(int[,] board)
        {
            CheckForRecurringNumbers(board);
            CheckForInvalidNumbers(board);
            return isNotValid;
        }

        private static void CheckForInvalidNumbers(int[,] board)
        {

            Parallel.For(0, Constants.BoardSize, (i, loopState) => Parallel.For(0, Constants.BoardSize, j =>
            {
                if (board[i, j] < 1 || board[i, j] > Constants.BoardSize)
                {
                    isNotValid = true;
                    loopState.Stop();   
                }
            }));
        }

        private static void CheckForRecurringNumbers(int[,] board)
        {
            isNotValid = false;

            Parallel.For(1, Constants.BoardSize+1, (i, loopState) =>
            {
                for (var j = 0; j < Constants.BoardSize; j++)
                {
                    //If current number being checked (i) has occurred more than once in a line/sub grid
                    //then raise the flag to say it's failed and break out of the loop
                    var currentNumberCount = 0;

                    for (var k = 0; k < Constants.BoardSize; k++)
                    {
                        if (board[j, k] == i)
                        {
                            currentNumberCount++;
                            if (currentNumberCount > 1)
                            {
                                isNotValid = true;
                                loopState.Stop();
                            }
                        }
                    }
                }

                for (var j = 0; j < Constants.BoardSize; j++)
                {
                    var c = 0;
                    for (var k = 0; k < Constants.BoardSize; k++)
                    {
                        if (board[k, j] == i)
                        {
                            c++;
                        }
                    }
                    if (c <= 1) continue;
                    isNotValid = true;
                    loopState.Stop();
                }

                if (isNotValid)
                {
                    loopState.Stop();
                }
                //Each 3x3 box is then checked to see if no reoccurring numbers appear
                var p = 0;
                var q = 0;
                for (var j = 0; j < Constants.BoardSize; j++)
                {
                    if (j < Constants.SubGridSize)
                    {
                        p += Constants.SubGridSize;
                        q = 0;
                    }

                    if (j >= Constants.SubGridSize && j < 2*Constants.SubGridSize)
                    {
                        p += Constants.SubGridSize;
                        q = Constants.SubGridSize;
                    }

                    if (j >= 2 * (Constants.SubGridSize) &&
                        j < 3 * (Constants.SubGridSize))
                    {
                        p += Constants.SubGridSize;
                        q = 2 * Constants.SubGridSize;
                    }
                    if (j % (Constants.SubGridSize) == 0)
                    {
                        p = 0;
                    }

                    var box = new int[Constants.SubGridSize, Constants.SubGridSize];

                    for (var k = 0; k < Constants.SubGridSize; k++)
                    {
                        for (var l = 0; l < Constants.SubGridSize; l++)
                        {
                            box[k, l] = board[k + p, l + q];
                        }
                    }
                    var aList = new List<int>();

                    //Hashset used to find if all items are unique in a list, if an item is already found, return false
                    var hashset = new HashSet<int>();
                    foreach (var item in box)
                    {
                        aList.Add(item);
                        if (!hashset.Add(item) && item != 0)
                        {
                            isNotValid = true;
                            loopState.Stop();
                        }
                    }
                    if (isNotValid)
                    {
                        loopState.Stop();
                    }
                }
                if (isNotValid)
                {
                    loopState.Stop();
                }
            });
        }
    }
}