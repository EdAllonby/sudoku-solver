﻿using System.Drawing;

namespace Sudoku_Solver
{
    /// <summary>
    /// Container for global constants
    /// </summary>
    internal static class Constants
    {
        public const int BoardSize = 9;
        public const int SubGridSize = 3;

        //Defined colours
        public static readonly Color PresetCellColour = Color.Yellow; //Cell that has been set by presets
        public static readonly Color UserChangedCellColour = Color.DeepSkyBlue; //Cell that has been set by user
        public static readonly Color SuccessColour = Color.LimeGreen; //When the Sudoku board is successfully completed
        public static readonly Color FailColour = Color.Tomato; //When the Sudoku board has failed to complete
        public static readonly Color QueryColour = Color.Orange; //Used for things in progress

    }
}