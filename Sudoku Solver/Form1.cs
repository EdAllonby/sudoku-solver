﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using gGlowBox;

namespace Sudoku_Solver
{
    public partial class Form1 : Form
    {
        private static readonly int[,] SudokuBoard = new int[Constants.BoardSize, Constants.BoardSize];

        private bool hasTheBoardBeenSolved;
        private bool shouldCellsBeEnabled;

        //Used for changing the speed/refresh rate of the algorithm
        public static int DelayBetweenIterations;
        public static int IterationResolution;

        //Turns on/off periodic GUI updates, from the solver class
        public static bool DoIterations;

        //Measures from start of algorithm to all GUI elements updated
        private readonly Stopwatch elapsedTime = new Stopwatch();

        //Used to run a method in the Main thread when the background worker is running
        private delegate void InvokeTextBoxEnabledDelegate();

        //Background worker created to run the solver algorithm in a separate thread to the GUI
        public static readonly BackgroundWorker WorkerThread = new BackgroundWorker
        {
            WorkerSupportsCancellation = true,
            WorkerReportsProgress = true
        };

        public Form1()
        {
            InitializeComponent();

            //Initialise values by calling TrackBar even handlers
            SleepTrackBarValueChanged(delayBetweenIterationsTrackBar, EventArgs.Empty);
            ResolutionTrackBarValueChanged(iterationResolutionTrackBar, EventArgs.Empty);

            ShowIterationsCheckedChanged(showIterationsCheckBox, EventArgs.Empty);

            boardToSolveGroup.GlowColor = Constants.SuccessColour;
            boardToSolveGroup.GlowOn = true;

            //Default board set to empty board
            presetBoardSelector.SelectedIndex = 2;

            //Set GUI to initial values of SudokuBoard
            SetSudokuCells(presetBoardSelector.SelectedIndex.SelectBoard());

            sudokuCompleteLabel.Text = "";
            validationResultsLabel.Text = "";

            //Create events handlers for background worker events
            WorkerThread.DoWork += BackgrounderWorkerThreadStart;
            WorkerThread.ProgressChanged += WorkerThreadUpdate;
            WorkerThread.RunWorkerCompleted += WorkerThreadCompleted;
        }

        private void SolvePuzzle()
        {
            //Set time to 0 and begin counter
            elapsedTime.Restart();

            //Updates GUI
            GetSudokuCells();

            Debug.WriteLine("Initial sudoku board: ");

            //Prints start state of sudokuBoard onto output logger
            SudokuBoard.PrintSolutionToConsole(newPuzzle: true);
            Debug.WriteLine("Start recursive stage: ");

            //Set flag to see if sudoku board can be solved
            hasTheBoardBeenSolved = SudokuBoard.Solve();
        }

        //Gets the values of the int[,] newSudokuBoard and sets them to the GUI Board (Text Boxes)
        private void SetSudokuCells(int[,] board)
        {
            foreach (var groupBox in Controls.OfType<gGlowGroupBox>())
            {
                int row = 0, column = 0;

                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    textBox.BackColor = SystemColors.Window;

                    textBox.Text = board[row, column].ToString(CultureInfo.InvariantCulture);
                    
                    if (textBox.Text != "0")
                    {
                        //If the cell is initially filled, set its color to yellow
                        textBox.BackColor = Constants.PresetCellColour;
                    }
                    if (column < Constants.BoardSize - 1)
                    {
                        column++;
                    }
                    else
                    {
                        column = 0;
                        row++;
                    }
                }
            }
        }

        private void SetEndStateCellColours(Color endStateColour)
        {
            foreach (var groupBox in Controls.OfType<gGlowGroupBox>())
            {
                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    textBox.BackColor = endStateColour;
                }
            }
        }

        //Sets the int[,] Board to the values of the TextBoxes (GUI Board)
        private void GetSudokuCells()
        {
            foreach (var groupBox in Controls.OfType<gGlowGroupBox>())
            {
                var row = 0;

                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    var textBoxNumber = Convert.ToInt32(Regex.Match(textBox.Name, @"\d+").Value) - 1;

                    var cellValue = 0;

                    if (textBox.Text != "")
                    {
                        cellValue = Convert.ToInt32(textBox.Text);
                    }

                    //When column number reaches the boardSize, wrap back to 0 and go down a row
                    if (Convert.ToInt32(textBoxNumber)%Constants.BoardSize == 0) //from string
                    {
                        row++;
                    }

                    //Parse text-boxes in WinForm into a 2D array with size of the board (i.e. 9x9)
                    SudokuBoard[row - 1, textBoxNumber%Constants.BoardSize] = cellValue;
                }
            }
        }

        private void SetInitialLabelState()
        {
            validationResultsLabel.ForeColor = Constants.QueryColour;
            validationResultsLabel.Text = "";

            Debug.WriteLine("Solving Puzzle...");
            sudokuCompleteLabel.ForeColor = Constants.QueryColour;
            sudokuCompleteLabel.Text = "Solving Puzzle...";
        }

        //Run this method when solver algorithm has finished
        private void SetCompleteLabelState()
        {
            elapsedTime.Stop();
            timeLabel.Text = elapsedTime.Elapsed.TotalMilliseconds + "ms";
            Color boardColour;
            iterationLabel.Text = Solver.Iteration.ToString("0,0", CultureInfo.InvariantCulture);

            if (hasTheBoardBeenSolved)
            {
                if (!Validation.DoChecks(SudokuBoard))
                {
                    Debug.WriteLine("Validation successful,");
                    validationResultsLabel.ForeColor = Constants.SuccessColour;
                    validationResultsLabel.Text = "Validation successful";

                    Debug.WriteLine("sudokuBoard solved!");
                    sudokuCompleteLabel.ForeColor = Constants.SuccessColour;
                    sudokuCompleteLabel.Text = "Board Solved!";

                    //Change cell background colours to green - Success
                    boardColour = Constants.SuccessColour;
                }
                else
                {
                    Debug.WriteLine("sudokuBoard solved!");
                    sudokuCompleteLabel.ForeColor = Constants.SuccessColour;
                    sudokuCompleteLabel.Text = "Board Solved!";

                    Debug.WriteLine("BUG!");
                    Debug.WriteLine("Program thought the sudokuBoard was solved, but validation was not successful!");
                    validationResultsLabel.ForeColor = Constants.FailColour;
                    validationResultsLabel.Text = "Bug! Validation unsuccessful";

                    //Change cell background colours to red - Failed
                    boardColour = Constants.FailColour;
                }
            }
            else
            {
                Debug.WriteLine("No solution found for this sudokuBoard configuration");
                sudokuCompleteLabel.ForeColor = Constants.FailColour;
                sudokuCompleteLabel.Text = "No solution found";

                //Change cell background colours to red - Failed
                boardColour = Constants.FailColour;
            }
            //Update colour of board
            SetEndStateCellColours(boardColour);
        }

        //Thread safe GUI calls
        private void InProgressGuiState()
        {
            solvePuzzleButton.Enabled = false;
            SetCellsEnabled();
        }

        //
        /// <summary>
        /// Iterates through the textboxes which reside in the groupbox
        /// sets them to whatever the shouldCellBeEnabled flag is
        /// </summary>
        private void SetCellsEnabled()
        {
            foreach (var textBox in Controls.OfType<gGlowGroupBox>()
                .SelectMany(groupBox => groupBox.Controls.OfType<TextBox>()))
            {
                textBox.Enabled = shouldCellsBeEnabled;
            }
        }

        /// <summary>
        /// Handler for when the solve button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolvePuzzle(object sender, EventArgs e)
        {
            SetInitialLabelState();
            WorkerThread.RunWorkerAsync();
        }

        /// <summary>
        /// Run the body of this method in the Background Worker thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgrounderWorkerThreadStart(object sender, DoWorkEventArgs e)
        {
            shouldCellsBeEnabled = false;

            //Invoke Main Thread to change certain control states whilst algorithm in running
            solvePuzzleButton.BeginInvoke(new InvokeTextBoxEnabledDelegate(InProgressGuiState));

            //Run the algorithm
            SolvePuzzle();
        }

        /// <summary>
        /// Used to update stuff on the GUI when the worker thread is running
        /// </summary>
        private void WorkerThreadUpdate(object sender, ProgressChangedEventArgs e)
        {
            iterationLabel.Text = Solver.Iteration.ToString("0,0", CultureInfo.InvariantCulture);

            //Update GUI in real time
            SetSudokuCells(SudokuBoard);
        }

        /// <summary>
        /// Runs when the worker method has finished its task
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkerThreadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            shouldCellsBeEnabled = true;
            SetCellsEnabled();
            solvePuzzleButton.Enabled = true;

            //Update GUI when algorithm has finished
            SetSudokuCells(SudokuBoard);

            SetCompleteLabelState();
        }

        /// <summary>
        /// Get 2D-Array from static class PresetBoards and set Update GUI to this
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPresetBoard(object sender, EventArgs e)
        {
            SetSudokuCells(presetBoardSelector.SelectedIndex.SelectBoard());
        }

        private void CellValidation(object sender, KeyPressEventArgs e)
        {
            var currentCell = sender as TextBox;

            //Only allow 
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            else if (currentCell != null)
            {
                currentCell.Text = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                if ("\b" == e.KeyChar.ToString(CultureInfo.InvariantCulture) ||
                    "\r" == e.KeyChar.ToString(CultureInfo.InvariantCulture) || 
                    27 == e.KeyChar)
                {
                    currentCell.Text = "0";
                }
                currentCell.BackColor = Constants.UserChangedCellColour;

                //Get focus on next cell
                GetNextCell(currentCell).Focus();
            }
        }

        /// <summary>
        /// Gets the name of the current cell, parses this and selects the cell with a name 1 greater than the current cell
        /// If current cell name is 81, wrap next cell back to 1
        /// </summary>
        /// <param name="currentCell">Currently selected cell as a control</param>
        /// <returns>Next cell to be selected as a control</returns>
        private TextBox GetNextCell(TextBox currentCell)
        {
            var nameValue = Int32.Parse(Regex.Match(currentCell.Name, @"\d+").Value);

            nameValue++;
            if (nameValue > Constants.BoardSize*Constants.BoardSize)    //boardsize*boardsize will give total amount of items in board
            {
                nameValue = 1;
            }

            var nextCell = Controls.Find("textBox" + nameValue, true).FirstOrDefault() as TextBox;
            return nextCell;
        }

        private void SleepTrackBarValueChanged(object sender, EventArgs e)
        {
            var movedTrackBar = sender as TrackBar;
            if (movedTrackBar != null)
            {
                DelayBetweenIterations = movedTrackBar.Value;
                delayBetweenIterationsLabel.Text = movedTrackBar.Value + " ms";
            }
        }

        private void ResolutionTrackBarValueChanged(object sender, EventArgs e)
        {
            var movedTrackBar = sender as TrackBar;
            if (movedTrackBar != null)
            {
                IterationResolution = movedTrackBar.Value;
                iterationResolutionLabel.Text = "1 / " + movedTrackBar.Value;
            }
        }

        private void ShowIterationsCheckedChanged(object sender, EventArgs e)
        {
            DoIterations = showIterationsCheckBox.Checked;

            delayBetweenIterationsTrackBar.Enabled = DoIterations;
            iterationResolutionTrackBar.Enabled = DoIterations;
        }
    }
}